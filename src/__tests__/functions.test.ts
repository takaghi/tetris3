import { getFlatFigure, turnFigure } from "../components/functions";

describe('functions', function () {
    test('getFlatFigure', function () {
        expect(getFlatFigure([[1, 0], [0, 1]], 2, 5)).toEqual([0, 0, 1, 0, 0, 0, 0, 0, 1, 0]);
        expect(getFlatFigure([[1]], 0, 3)).toEqual([1, 0, 0]);
    })

    test('turnFigure', function () {
        expect(turnFigure([[1, 0], [0, 1]])).toEqual([[0, 1], [1, 0]]);
        expect(turnFigure([
            [1,0],
            [1,0],
            [1,1],
        ])).toEqual([[1, 1, 1], [1, 0, 0]]);
    })
})

export {}