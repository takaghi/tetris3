import { memo } from "react"
import { cellWidth } from "./settings"

const Cell = memo(({value, index}: {value:number, index: number}) => {
    return <div className={`item item_${value}`} 
    style={{width: cellWidth, height:cellWidth}}/>
})

export default Cell