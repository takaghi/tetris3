import { memo } from "react"
import Digits from "./Digits"

const figureScore = 10
const lineScore = 100

type State = {
    figures: number,
    lines: number,
    total: number
}

type ScoreType = {
    state: State
}

export const scoreInitial = {
    figures: 0,
    lines: 0,
    total: 0
}

export const scoreReducer = (state:State, action:{type: string, amount?: number}) => {
    if (action.type === 'figure') {
        return {
            ...state,
            figures: state.figures + 1,
            total: state.total + figureScore
        }
    } else if (action.type === 'lines') {
        const amount = action.amount ?? 1
        return {
            ...state,
            lines: state.lines + amount,
            total: state.total + amount * lineScore + lineScore * (amount - 1)
        }
    } else if (action.type === 'reset') {
        return {
            ...state,
            figures: 0,
            lines: 0,
            total: 0
        }
    }
    return state
}

const Score = memo(({state}:ScoreType) => {
    return (
        <div className="score">
            {/* <div>{state.figures}</div>
            <div>{state.lines}</div>
            <div>{state.total.toString()}</div> */}
            <div><Digits value={state.total}/></div>
        </div>
    )
})

export default Score