import { useCallback, useEffect, useRef } from "react";

export const useInterval = (callback:() => void, speed:number) => {
    const savedCallback = useRef(callback);
    const animationRef = useRef(0)
    const lastTime = useRef(0)
  
    useEffect(() => {
        savedCallback.current = callback
    })

    const animation = useCallback((time:number) => {
        if (time - lastTime.current > 1000 / speed) {
            lastTime.current = time
            savedCallback.current();
        }
        
        animationRef.current = requestAnimationFrame(animation)
    }, [speed])
  
    useEffect(() => {
        animationRef.current = requestAnimationFrame(animation)
        return () => cancelAnimationFrame(animationRef.current);
    }, [animation]);
}