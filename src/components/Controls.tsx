import { useEffect } from "react"
import { isTouchDevice } from "./functions"

type ControlsType = {
    onkeyup: (event:KeyboardEvent | React.TouchEvent) => void
    onkeydown: (event:KeyboardEvent | React.TouchEvent) => void
}

const Controls = ({onkeydown, onkeyup}:ControlsType) => {
    useEffect(() => {
        document.addEventListener('keydown', onkeydown)
        document.addEventListener('keyup', onkeyup)

        return () => {
            document.removeEventListener('keydown', onkeydown)
            document.removeEventListener('keyup', onkeyup)
        }
    }, [onkeydown, onkeyup])

    return ((isTouchDevice() && 
            <div className="touchControls">
                <div>
                    <button onTouchStart={onkeydown} data-key="ArrowLeft">⇦</button>
                    <button onTouchStart={onkeydown} onTouchEnd={onkeyup} data-key="ArrowDown">⇩</button>
                </div>
                <div>
                    <button onTouchStart={onkeydown} data-key="ArrowRight">⇨</button>
                    <button onTouchStart={onkeydown} data-key="ArrowUp">◉</button>
                </div>
            </div>) || null
    )
}

export default Controls