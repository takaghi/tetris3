import { memo } from "react"
import Digit from "./Digit"

const Digits = memo(({value, color = '#42d6a4'}: {value:number, color?: string}) => {
    return <>{value.toString().split('').map((d, index) => <Digit key={index} color={color} value={Number(d)}/>)}</>
})

export default Digits