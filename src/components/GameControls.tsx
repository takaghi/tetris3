type GameControlsType = {
    gameStart: () => void
    gameToggle: () => void
    speed: number
}

const GameControls = ({gameStart, gameToggle, speed}:GameControlsType) => {
    return (
        <div className="restart">
            <button onClick={gameStart}>start</button>
            <button onClick={gameToggle}>{!speed ? 'resume' : 'pause'}</button>
        </div>
    )
}

export default GameControls