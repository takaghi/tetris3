import { memo } from "react";
import Cell from "./Cell";

const NextFigure = memo(({figure} : {figure:number[][]}) => {
    if (!figure || !figure[0]) {
        return null
    }

    return (
        <div className="nextFigure">
            <div className={'box'} style={{ gridAutoColumns: 0, gridTemplateColumns: `repeat(${figure[0].length}, 1fr)`, gridTemplateRows: `repeat(${figure.length}, 1fr)`, margin: 0, flexShrink: 0}}>
                {
                    figure.flat().map((value:number, index:number) => <Cell key={index} index={index} value={value}/>)
                }
            </div>
        </div>
    )
})

export default NextFigure