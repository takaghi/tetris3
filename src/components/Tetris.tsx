import {useCallback, useEffect, useReducer, useRef, useState} from "react";
import Cell from "./Cell";
import Controls from "./Controls";
import { fixMatrix, getFieldMatrix, getNewFigure, mergeMatrix, moveFigure, randomLimit, testFigureX, turnFigure } from "./functions";
import GameControls from "./GameControls";
import { useInterval } from "./hooks";
import NextFigure from "./NextFigure";
import Score, { scoreInitial, scoreReducer } from "./Score";
import { downSpeed, HEIGHT, initSpeed, scoreSpeedFactor, WIDTH } from "./settings";


const Tetris = () => {
    const [x, setX] = useState(randomLimit(WIDTH - 1))
    const [y, setY] = useState(0)
    const [matrix, setMatrix] = useState<number[]>(getFieldMatrix())

    const [speed, setSpeed] = useState(initSpeed)
    const currentSpeedRef = useRef(initSpeed)

    const [figure, setFigure] = useState([] as number[][])
    const nextFigureRef = useRef([] as number[][])

    const [scoreState, scoreDispatch] = useReducer(scoreReducer, scoreInitial)

    const matrixRef = useRef(matrix)

    useInterval(() => {
        setY(y => y + 1)
    }, speed);

    const updateSpeed = useCallback((newSpeed:number) => {
        currentSpeedRef.current = newSpeed
        setSpeed(currentSpeedRef.current)
    }, [])

    const newFigure = useCallback(() => {
        if (!nextFigureRef.current.length) {
            nextFigureRef.current = getNewFigure()
        }
        setY(0)
        const newFig = nextFigureRef.current
        setX(randomLimit(WIDTH - newFig[0].length + 1))
        setFigure(newFig)
        nextFigureRef.current = getNewFigure()
    }, [])

    const checkLines = useCallback(() => {
        let matrix = matrixRef.current as number[]
        let rows:number[] = [],
            count = WIDTH
        for (let i = 0; i < matrix.length; i++) {
            if (matrix[i] > 0) {
                count--
            }

            if ((i + 1) % WIDTH === 0) {
                if (!count) {
                    rows.push(i / WIDTH | 0)
                }
                count = WIDTH
            }
        }

        for (const row of rows) {
            matrix.splice(row*WIDTH, WIDTH)
            matrix.unshift(...new Array(WIDTH).fill(0))
        }

        
        if (rows.length) {
            setMatrix(matrix)
            scoreDispatch({type: 'lines', amount: rows.length})
        }
    }, [])

    useEffect(newFigure, [newFigure])

    const gameOver = useCallback(() => {
        updateSpeed(0)
    }, [updateSpeed])

    const gameToggle = useCallback(() => {
        if (!speed) {
            setSpeed(currentSpeedRef.current)
        } else {
            setSpeed(0)
        }
    }, [speed])

    const gameStart = () => {
        scoreDispatch({type: 'reset'})
        matrixRef.current = getFieldMatrix()
        setMatrix(matrixRef.current)
        setY(0)
        updateSpeed(initSpeed)
    }

    useEffect(() => {
        const liveMatrix = moveFigure(figure, x, y)
        const mergedMatrix = mergeMatrix(liveMatrix, matrixRef.current)
        if (!mergedMatrix.length) {
            if (!y) {
                gameOver()
                return
            }
            scoreDispatch({type: 'figure'})
            matrixRef.current = fixMatrix(matrixRef.current)
            setMatrix(matrixRef.current)
            newFigure()
            checkLines()
        } else {
            setMatrix(mergedMatrix)
        }
    }, [checkLines, figure, gameOver, newFigure, x, y])

    const updateX = useCallback((dir:number) => {
        const newX = x + dir
        if (newX >= 0 && newX <= WIDTH - figure[0].length && testFigureX(matrixRef.current, figure, newX, y)) {
            setX(newX)
        }
    }, [figure, x, y])

    const onkeydown = useCallback((event:KeyboardEvent | React.TouchEvent) => {
        const key = ('key' in event && event.key) || (event.target as HTMLButtonElement).dataset.key

        if (key === 'ArrowLeft') {
            updateX(- 1)
        } else if (key === 'ArrowRight') {
            updateX(1)
        } else if (key === 'ArrowUp') {
            setFigure(turnFigure(figure))
        } else if (key === 'ArrowDown') {
            setSpeed(downSpeed)
        }
    }, [figure, updateX])

    const onkeyup = useCallback((event:KeyboardEvent | React.TouchEvent) => {
        const key = ('key' in event && event.key) || (event.target as HTMLButtonElement).dataset.key
        if (key === 'ArrowDown') {
            setSpeed(currentSpeedRef.current)
        }
    }, [])

    useEffect(() => {
        matrixRef.current = matrix
    }, [matrix])

    useEffect(() => {
        if (!currentSpeedRef.current) {
            return
        }
        if (scoreState.total < scoreSpeedFactor) {
            updateSpeed(initSpeed)
        } else {
            updateSpeed(initSpeed + (scoreState.total / scoreSpeedFactor | 0) / 5)
        }
    }, [scoreState, updateSpeed])

    return (
        <>
            <div tabIndex={0} style={{outline: 0, display:'flex', alignItems:'flex-start', justifyContent:'center', marginTop: 20}}>
                <div className="scoreState">
                    <Score state={scoreState}/>
                    <div style={{fontSize: '50%'}}>{currentSpeedRef.current}</div>
                </div>
                <div className={'box'} style={{  gridTemplateColumns: `repeat(${WIDTH}, 1fr)`, gridTemplateRows: `repeat(${HEIGHT}, 1fr)`}}>
                    {
                        matrix.map((value:number, index:number) => <Cell key={index} index={index} value={value}/>)
                    }
                </div>
                <NextFigure figure={nextFigureRef.current}/>
                {/* <div>
                    {FIGURES.map((fig, index) => <div style={{marginBottom: 20}} key={index} onClick={() => {newFigure(index);setY(0)}}>{JSON.stringify(fig)}</div>)}
                </div> 
                
                <button onClick={() => setY(y + 1)}>{y}</button>
                <button onClick={() => updateX(-1)}>{-x}</button>
                <button onClick={() => updateX(1)}>{+x}</button> */}
            </div>

            <GameControls gameStart={gameStart} gameToggle={gameToggle} speed={speed}/>
            <Controls onkeydown={onkeydown} onkeyup={onkeyup}/>
        </>
    )
}

export default Tetris
