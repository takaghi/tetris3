import { FIGURES, HEIGHT, WIDTH } from "./settings";

const fixedValue = 100

export const isTouchDevice = () => 'ontouchstart' in window || navigator.maxTouchPoints > 0

const rand = function () {
    let last = 0
    return (limit:number) => {
        let random = Math.floor(Math.random() * limit)
        do {
            random = Math.floor(Math.random() * limit)
        } while (last === random)
        last = random
        return random
    }
}

export const randomLimit = rand()
export const randomFigure = rand()

export const getFieldMatrix = () => new Array(WIDTH*HEIGHT).fill(0)

const mapMatrix = (matrix: number[][], func:(i:number) => number) => {
    const newMatrix = matrix.map(arr => arr.slice())
    for (let i = 0; i < newMatrix.length; i++) {
        for (let j = 0; j < newMatrix[i].length; j++) {
            newMatrix[i][j] = func(matrix[i][j])
        }
    }

    return newMatrix
}

export const getNewFigure = (index = -1) => {
    const figIndex = index >= 0 ? index : randomFigure(FIGURES.length);
    if (!FIGURES[figIndex]) {
        return []
    }
    const figure = mapMatrix(FIGURES[figIndex], (value) => value * (figIndex + 1))
    return figure
}

export const turnFigure = (figure: number[][]) => {
    return figure[0].map((val, index) => figure.map(row => row[index]).reverse())
}

export const getFlatFigure = (figure: number[][], x:number, width = WIDTH) => {
    if (!figure.length) {
        return []
    }
    if (x > width - figure[0].length) {
        x = width - figure[0].length
    }
    const flatFigure = []
    for (const row of figure) {
        flatFigure.push(...new Array(x).fill(0), ...row, ...new Array(width - x - row.length).fill(0))
    }
    return flatFigure
}

export const testFigureX = (matrix: number[], figure: number[][], x:number, y: number, width = WIDTH) => {
    const flatFigure = getFlatFigure(figure, x)
    for (let i = 0; i < flatFigure.length; i++) {
        if (matrix[y * width + i] + flatFigure[i] > fixedValue) {
            return false
        }
    }

    return true
}

export const moveFigure = (figure: number[][], x:number, y: number, width = WIDTH) => {
    const flatFigure = getFlatFigure(figure, x)
    const newMatrix = getFieldMatrix()

    for (let i = 0; i < flatFigure.length; i++) {
        newMatrix[y * width + i] += flatFigure[i]
    }

    return newMatrix
}

export const mergeMatrix = (liveMatrix: number[], fixedMatrix: number[]) => {
    const newMatrix = [...fixedMatrix]
    
    for (let i = 0; i < liveMatrix.length; i++) {
        newMatrix[i] = newMatrix[i] < fixedValue ? liveMatrix[i] : newMatrix[i] + liveMatrix[i]
        if (isNaN(newMatrix[i]) || newMatrix[i] > fixedValue) {
            return []
        }
    }

    return newMatrix
}

export const fixMatrix = (matrix: number[]) => matrix.map(i => i < fixedValue ? limitValue(i * fixedValue) : i)

const limitValue = (value:number) => value > fixedValue ? fixedValue : value
