export const cellWidth = window.innerWidth < 700 ? window.innerWidth < 400 ? 25 : 30 : 40
export const initSpeed = 2
export const downSpeed = 40
export const scoreSpeedFactor = 500

export const WIDTH = window.innerWidth < 700 ? window.innerWidth * 0.6 / cellWidth | 0 : 20 / (cellWidth/ 20) | 0
export const HEIGHT = window.innerHeight * 0.85 / cellWidth | 0

export const FIGURES = [
    [
        [1,0],
        [1,0],
        [1,1],
    ],
    [
        [0,1],
        [0,1],
        [1,1],
    ],
    [
        [1,1,0],
        [0,1,1],
    ],
    [
        [0,1,1],
        [1,1,0],
    ],
    [
        [0,1,0],
        [1,1,1],
    ],
    [
        [1],
        [1],
        [1],
        [1],
    ],
    [
        [1, 1],
        [1, 1],
    ]
]